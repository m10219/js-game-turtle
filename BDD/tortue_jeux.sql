-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 02 oct. 2022 à 18:01
-- Version du serveur :  5.7.26
-- Version de PHP :  5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tortue_jeux`
--

-- --------------------------------------------------------

--
-- Structure de la table `score`
--

DROP TABLE IF EXISTS `score`;
CREATE TABLE IF NOT EXISTS `score` (
  `ID` int(4) NOT NULL AUTO_INCREMENT,
  `USER` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SCORE` int(4) NOT NULL,
  `DATE` datetime(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER` (`USER`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `score`
--

INSERT INTO `score` (`ID`, `USER`, `SCORE`, `DATE`) VALUES
(58, 'Jean-Kevin', 19, '2022-10-02 18:24:29.0'),
(57, 'Eric', 379, '2022-10-02 18:24:13.0'),
(56, 'Samantha', 279, '2022-10-02 18:24:04.0'),
(50, 'Arthur', 31, '2022-10-02 01:22:44.0'),
(55, 'Valentin', 229, '2022-10-02 18:23:53.0'),
(54, 'Pierre', 29, '2022-10-02 18:23:38.0'),
(53, 'Alexandre', 666, '2022-10-02 03:14:17.0'),
(52, 'Arthur', 44, '2022-10-02 03:10:51.0'),
(19, 'Arthur', 31, '2022-10-01 17:32:23.0'),
(20, 'Arthur', 31, '2022-10-01 17:33:39.0'),
(65, 'Alexandre', 39, '2022-10-02 19:46:37.0'),
(66, 'Alexandre', 24, '2022-10-02 19:48:11.0'),
(63, 'Eric', 24, '2022-10-02 19:19:39.0'),
(62, 'Samantha', 32, '2022-10-02 19:16:35.0'),
(61, 'Pierre', 65, '2022-10-02 19:07:31.0'),
(60, 'Pierre', 31, '2022-10-02 19:06:14.0'),
(59, 'Coline', 219, '2022-10-02 18:24:44.0');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
