//capture DOM element
const character =
    document.getElementById("character");
const game = document.getElementById("game");
const block = document.getElementById("block");
const score = document.getElementById("score");
const gameoverElement = document.getElementById("gameover");
const scorelist = document.getElementById("scorelist");
const myselect = document.getElementById("myselect");

//jQuery selector
const scorelist1 = $("#scorelist");
const character1 = $("#character");
const menu1 = $("#menu");
const gameover1 = $("#gameover");
const instructions1 = $("#instructions");
const game1 = $("#game");
const life1 = $(".life");

//player
const selectElement = document.querySelector('#myselect');
let currentPlayer = undefined

// variable de jeu
let gameOver = 0;
let scoreCounter = 0;

// music
const audio = new Audio("media/Komiku_-_12_-_Bicycle.mp3");
audio.loop = true
const deathMusic = new Audio("media/Mario Death - QuickSounds.com.mp3");
const jumpSound = new Audio("media/Mario-jump-sound.mp3");
jumpSound.volume = 0.3;
const welcomeSound = new Audio("media/BlackSnaiL - Gem X.mp3");
welcomeSound.loop = true


function jump() {
    jumpSound.play();
    if (!character1.hasClass("jump")) {
        character.classList.replace("run", "jump");
        character1.css({'width': '100px'});
    }
    setTimeout(function () {
        if (!character1.hasClass("dead")) {
            character.classList.replace("jump", "run");
            character1.css({'width': '88px'});
        }
    }, 1300);
}

function run() {

    if (!currentPlayer) {
        currentPlayer = myselect.value
        // $('#myselect').val(); // la valeur en jQuery
        // $('#myselect :selected').text(); // le texte
    }
    welcomeSound.pause()
    if (!character1.hasClass("run")) {
        display('game')
        character.classList.remove("dead", "jump");
        character.classList.add("run");
        character1.css({'width': '88px'});
        character1.css('background-image', "");
    }
    audio.currentTime = 0;
    audio.play();
    setTimeout(function () {
        moving()
    }, 500);
}

function moving() {
    $('#block img').remove();
    $('#block').append("<img src=\"media/stone_tallB_SW.png\" class='rock'  alt=\"stone\" width=\"30px\">");
    $("#block").attr('style', 'display : block');
    block.classList.add("moving");
}

function pauseGame() {
    audio.pause();
    character.classList.remove("run", "jump");
    block.classList.remove("moving")
    $("#block").attr('style', 'display : none');
    character1.css({
        'background-image': 'url("tortue-pixel/tortue_1.png")'
    });
    // $("#block").attr('style', 'display : block');
    //     parseInt(window.getComputedStyle(block).getPropertyValue("left"));
    // $('#block').attr('style', 'left : ' + test + 'px');
}

function dead() {
    character.classList.remove("run", "jump");
    character.classList.add("dead");
    character1.css({
        'background-image': 'url("tortue-pixel/tortue_dead.png")',
        'width': '112px'
    });
    audio.pause()
    deathMusic.play();
    gameOver += 1;
    if (gameOver === 1) {
        $("#life3").addClass("loseLife")
        setTimeout(function () {
            $("#life3").css('display', 'none')
        }, 1500);
    } else if (gameOver === 2) {
        $("#life2").addClass("loseLife")
        setTimeout(function () {
            $("#life2").css('display', 'none')
        }, 1500);
    } else if (gameOver === 3) {
        $("#life1").addClass("loseLife")
        setTimeout(function () {
            $("#life1").css('display', 'none')
        }, 1500);
        sendScoring();
        setTimeout(function () {
            let scoreFinal = parseInt(scoreCounter)
            $('#gameover').html("GAME OVER<br>Your Score : " + scoreFinal + "<br><br><p>PLAY AGAIN<br>[PRESS ENTER]</p>")
            display('gameover')
        }, 1500);
    }
}

function newparty() {

    life1.css('display', 'block')
    life1.removeClass('loseLife')
    character1.append('<img src')
    character1.css({
        'background-image': 'url("tortue-pixel/tortue_1.png")',
        'display': 'block'
    });
    scoreCounter = 0
    gameOver = 0
    run();
}

$(document).keypress(function (event) {

    if (event.code === 'Enter' || event.key === 'z' || event.key === 'Z') {
        if (character1.hasClass("run")) {
            pauseGame();
        } else if (gameOver === 3) {
            playGame();
        } else if (game.style.display == "block") {
            run();
        }
    }
    if ((event.code === 'Space' || event.key === 'j' || event.key === 'J') && character1.hasClass("run")) {
        jump()
    }
    if (event.key === 'm' || event.key === 'M') {
        menu()
    }
    if (event.key === 's' || event.key === 'S') {
        getData('score')
    }
    if (event.key === 'p' || event.key === 'P') {
        playGame()
    }

});

// Comptage Score et Collision
const checkDead = setInterval(function () {
    let characterTop =
        parseInt(window.getComputedStyle(character).getPropertyValue("top"));
    let characterBottom =
        parseInt(window.getComputedStyle(character).getPropertyValue("bottom"));
    let blockLeft =
        parseInt(window.getComputedStyle(block).getPropertyValue("left"));
    let blockRight =
        parseInt(window.getComputedStyle(block).getPropertyValue("left"));

    if (blockLeft < 70 && blockLeft > 50 && characterBottom <= 32) {
        block.style.animation = "none";
        block.style.display = "none";
        dead();
    }
    if (character1.hasClass("run") || character1.hasClass("jump")) {
        scoreCounter += 0.05
    }
    if (game.style.display == "block") {
        score.innerHTML = "SCORE : " + parseInt(scoreCounter);
    }
    if (game.style.display != "block") {
        welcomeSound.play()
    }
}, 10);


// AJAX REQUEST
const getHttpRequest = function () {
    let httpRequest = false;

    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        httpRequest = new XMLHttpRequest();
        if (httpRequest.overrideMimeType) {
            httpRequest.overrideMimeType('text/xml');
        }
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {

            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {

            }
        }
    }
    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance XMLHTTP');
        return false;
    }
    return httpRequest
};


function sendScoring() {

    if (!currentPlayer) {
        return;
    }
    const data = new FormData();
    data.append('user', currentPlayer);
    data.append('score', parseInt(scoreCounter));

    const xhr = getHttpRequest();
    xhr.open('POST', 'postData.php', true)
// On envoit un header pour indiquer au serveur que la page est appellée en Ajax
    xhr.setRequestHeader('X-Requested-With', 'xmlhttprequest')
// On lance la requête
    xhr.send(data)

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                xhr.responseText // contient le résultat de la page
            } else {
                // Le serveur a renvoyé un status d'erreur
            }
        }
    }
}

let objData = undefined
let jj = undefined
// récupére toute la table depuis la BDD
function getData(code) {

    const xhr = getHttpRequest();
    xhr.open('GET', 'getData.php', true)
    xhr.setRequestHeader('X-Requested-With', 'xmlhttprequest')
    xhr.send();

    xhr.onreadystatechange = function () {
        if ((xhr.readyState === 4) && (xhr.status === 200)) {
            jj=xhr.responseText
            objData = JSON.parse(xhr.responseText);
            if (code === 'score') {
                displayScore(objData)
            } else if (code === 'players') {
                displayPlayers(objData)
            }
        }
    }
}
// afficher les scores
function displayScore(objTab) {

    if (scorelist.style.display != "block") {

        pauseGame()
        scorelist1.empty();
        display('scorelist')

        scorelist1.css('display', 'block')
        scorelist1.append("<p><mark>BEST SCORES</mark></p><br>");
        scorelist1.append("<ul></ul>");

        let key = 0;
        while (key < 10) {
            $("#scorelist ul").append("<li>" + objTab[key].USER + " : " + objTab[key].SCORE + "</li>");
            key++;
        }
    }
}

let checkPlayers = []

// lister les players
function displayPlayers(objTab) {

    checkPlayers = []

    if (myselect.value) {return}

    objTab.forEach((item) => {
        if (!checkPlayers.includes(item.USER)) {
            $("#myselect").append("<option value=" + item.USER + ">" + item.USER + "</option>");
            checkPlayers.push(item.USER)
        }
    })
}

// SELECT PLAYER
getData('players');

selectElement.addEventListener('change', (event) => {
    currentPlayer = `${event.target.value}`;
});

//AFFICHAGE
function menu() {
    pauseGame()
    display('menu')
}

function playGame() {
    display('game')
    newparty();
}

function howtoplay() {
    display('instructions')
}


function display(id) {

    menu1.css('display', 'none')
    scorelist1.css('display', 'none')
    gameover1.css('display', 'none')
    instructions1.css('display', 'none')
    game1.css('display', 'none')

    switch (id) {
        case 'menu':
            menu1.css('display', 'block')
            break;
        case 'scorelist':
            scorelist1.css('display', 'block')
            break;
        case 'gameover':
            gameover1.css('display', 'block')
            break;
        case 'instructions':
            instructions1.css('display', 'block')
            break;
        case 'game':
            game1.css('display', 'block')
            break;
        default:
            break;
    }
}

//MUSIC ON OFF
function soundOFF() {
    jumpSound.volume = 0;
    audio.volume = 0;
    deathMusic.volume = 0;
    welcomeSound.volume = 0;
}

function soundON() {
    jumpSound.volume = 0.3;
    audio.volume = 1;
    deathMusic.volume = 1;
    welcomeSound.volume = 1;
}

$("#sound img").click(function () {
    if (audio.volume === 1) {
        $("#sound img").attr('src', '/media/sound.ON.png')
        soundOFF()
    } else {
        $("#sound img").attr('src', '/media/sound.OFF.png')
        soundON()
    }
});